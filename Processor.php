<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2018 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\HouseKeepingPlugin;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use TheRealWorld\HouseKeepingPlugin\ShopMaintenance;

class Processor implements PluginInterface
{
    /**
    * Composer io and compser obj
    * @type obj;
    */
    protected static $io;
    protected static $composer;

    /**
    * Register shop packages installer.
    *
    * @param Composer    $composer
    * @param IOInterface $io
    */
    public function activate(Composer $composer, IOInterface $io)
    {
        self::$composer = $composer;
        self::$io = $io;
    }

    /**
    * preRun Wrapper for ShopMaintenance
    */
    public static function preRun()
    {
        ShopMaintenance::setComposer(self::$composer);
        ShopMaintenance::setIo(self::$io);
        if (
            ($mIsInstallationPossible = ShopMaintenance::isInstallationPossible()) === true
        ) {
            ShopMaintenance::setShopMaintenanceOn();
            ShopMaintenance::disableActiveModules();
            ShopMaintenance::deleteModulesBeforeInstall();
            ShopMaintenance::deleteThemesBeforeInstall();
        } else {
            self::$io->write($mIsInstallationPossible);
        }
    }

    /**
    * postRun Wrapper for ShopMaintenance
    */
    public static function postRun()
    {
        ShopMaintenance::setComposer(self::$composer);
        ShopMaintenance::setIo(self::$io);
        if (
            ($mIsInstallationPossible = ShopMaintenance::isInstallationPossible()) === true
        ) {
            ShopMaintenance::deleteDemoImages();
            ShopMaintenance::activateActiveModules();
            ShopMaintenance::setShopMaintenanceOff();
        } else {
            self::$io->write($mIsInstallationPossible);
        }
    }
}
