Composer Plugin for Cleanup a OXID6 Installation during composer-install or updatePackage
======

![vendor-logo the-real-world.de](picture.png)

### description

The Plugin ...

* activate the maintenance-Mode from shop before Installation. (ShopMaintenance::setShopMaintenanceOn)
* disable all active Modules before Installation. (ShopMaintenance::disableActiveModules)
    * If you have modules that are interdependent and there are modules that need to be activated before
      all other modules, you can note these VIP modules here in the order of installation.
      All other modules are then activated in the order supplied by OXID. ("housekeeping-vip-modules")
* deletes the unessessary demo-images from OXID6 source. (ShopMaintenance::deleteDemoImages)
    * This can be dropped once the OXID6 developers remove the demo images from the
      ["oxideshop_ce repository"](https://github.com/OXID-eSales/oxideshop_ce.git)
* removes the shop modules before they are copied anew by the composer-installation.
  This prevents the inquiry as to whether the modules should be overwritten. (ShopMaintenance::deleteModulesBeforeInstall)
  You can define the vendor-paths of your modules that you want to delete
  (extra->therealworld-parameters->housekeeping-delete-modulevendorids)
* removes the shop themes before they are copied anew by the composer-installation.
  This prevents the inquiry as to whether the theme should be overwritten. (ShopMaintenance::deleteThemesBeforeInstall)
  You can define the theme-ids of your themes that you want to delete
  (extra->therealworld-parameters->housekeeping-delete-themeids)
* activate all active Modules after Installation. (ShopMaintenance::activateActiveModules)
* deactivate the maintenance-Mode from shop after Installation. (ShopMaintenance::setShopMaintenanceOff)
* would ask before cleaning the modules and themes. If you do not want to be asked, set the switch
  "housekeeping-delete-withoutprompt" to true
  (extra->therealworld-parameters->"housekeeping-delete-withoutprompt":true)
* if a defined file exists, the installation is cancelled. This file could be create by another composer plugin
  (extra->therealworld-parameters->"housekeeping-dont-run-if-file-exists"->XYZ)
* if a defined file not exists, the installation is cancelled. This file could be create by another composer plugin
  (extra->therealworld-parameters->"housekeeping-run-if-file-exists"->XYZ)
* if a oxid-bool-variable are not true, the installation is cancelled.
  (extra->therealworld-parameters->"housekeeping-dont-run-if-boolconf-true"->XYZ)
* if a oxid-bool-variable are true, the installation is cancelled.
  (extra->therealworld-parameters->"housekeeping-run-if-boolconf-true"->XYZ)
* you can call all steps individually or run a wrapper (Processor::preRun, Processor::postRun)

### add to your oxid6 composer.json

```
	"require": {

		...

		"therealworld/housekeeping-plugin": "^1.4"

		...

	}

	...

	"scripts": {
		"pre-install-cmd": [

			...

			"TheRealWorld\\HouseKeepingPlugin\\Processor::preRun",
/*
* alternatively all steps individually
*
* 			"TheRealWorld\\HouseKeepingPlugin\\ShopMaintenance::setShopMaintenanceOff",
* 			"TheRealWorld\\HouseKeepingPlugin\\ShopMaintenance::disableActiveModules",
* 			"TheRealWorld\\HouseKeepingPlugin\\ShopMaintenance::deleteModulesBeforeInstall",
* 			"TheRealWorld\\HouseKeepingPlugin\\ShopMaintenance::deleteThemesBeforeInstall",
*/
			...

		],
		"post-install-cmd": [

			...

			"TheRealWorld\\HouseKeepingPlugin\\Processor::postRun",
/*
* alternatively all steps individually
*
* 			"TheRealWorld\\HouseKeepingPlugin\\ShopMaintenance::deleteDemoImages",
* 			"TheRealWorld\\HouseKeepingPlugin\\ShopMaintenance::activateActiveModules",
* 			"TheRealWorld\\HouseKeepingPlugin\\ShopMaintenance::setShopMaintenanceOff"
*/
			...

		]
	},

	...

	"extra": {

		...

		"therealworld-parameters": {
			"housekeeping-delete-modulevendorids": [
				"trw", "bs", "oe"
			],
			"housekeeping-delete-themeids": [
				"flowdevelop", "flow"
			],
			"housekeeping-vip-modules": [
				"trwtools", "trwscheduler"
			],
			"housekeeping-dont-run-if-file-exists": [
				"dontinstall"
			],
			"housekeeping-dont-run-if-file-not-exists": [
				"pleaseinstall"
			],
			"housekeeping-dont-install-if-boolconf-true": [
				"varxyz"
			],
			"housekeeping-dont-install-if-boolconf-false": [
				"varabc"
			],
			"housekeeping-delete-withoutprompt": true
		}

		...

	}
```