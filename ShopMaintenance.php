<?php

/**
* @author    Mario Lorenz, www.the-real-world.de
* @copyright 2018 the-real-world.de
* @license   https://www.gnu.org/licenses/gpl-2.0.html GNU General Public License, version 2 (one or other)
*/

namespace TheRealWorld\HouseKeepingPlugin;

use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\ConfigFile;
use OxidEsales\Eshop\Core\UtilsObject;
use OxidEsales\Eshop\Core\Module\Module;
use OxidEsales\Eshop\Core\Module\ModuleCache;
use OxidEsales\Eshop\Core\Module\ModuleInstaller;
use OxidEsales\Eshop\Core\Module\ModuleList;

define('INSTALLATION_ROOT_PATH', getcwd() . DIRECTORY_SEPARATOR);
define('OX_BASE_PATH', INSTALLATION_ROOT_PATH . DIRECTORY_SEPARATOR . 'source' . DIRECTORY_SEPARATOR);
define('OX_LOG_FILE', OX_BASE_PATH . 'log' . DIRECTORY_SEPARATOR . 'oxideshop.log');
define('OX_OFFLINE_FILE', OX_BASE_PATH . 'offline.html');
define('VENDOR_PATH', INSTALLATION_ROOT_PATH . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR);
define('CONFIG_FILE', OX_BASE_PATH . "config.inc.php");

define('VENDOR_PICTURE_PATH', VENDOR_PATH .
    'oxid-esales' . DIRECTORY_SEPARATOR .
    'oxideshop-ce' . DIRECTORY_SEPARATOR .
    'source' . DIRECTORY_SEPARATOR .
    'out' . DIRECTORY_SEPARATOR .
    'pictures' . DIRECTORY_SEPARATOR);

define('SOURCE_PICTURE_PATH', OX_BASE_PATH .
    'out' . DIRECTORY_SEPARATOR .
    'pictures' . DIRECTORY_SEPARATOR);

define('SOURCE_MODULE_PATH', OX_BASE_PATH .
    'modules' . DIRECTORY_SEPARATOR);

define('SOURCE_THEME_PATH', OX_BASE_PATH .
    'Application' . DIRECTORY_SEPARATOR .
    'views' . DIRECTORY_SEPARATOR);

define('SOURCE_OUT_PATH', OX_BASE_PATH .
    'out' . DIRECTORY_SEPARATOR);

define('DIFFERENCE_PATH', DIRECTORY_SEPARATOR .
    'vendor' . DIRECTORY_SEPARATOR .
    'oxid-esales' . DIRECTORY_SEPARATOR .
    'oxideshop-ce');

/**
* Where CORE_AUTOLOADER_PATH points depends on how OXID eShop has been installed. If it is installed as part of a
* compilation, the directory 'Core', where the auto load classes are located, does not reside inside OX_BASE_PATH,
* but inside VENDOR_PATH.
*/
if (!is_dir(OX_BASE_PATH . 'Core')) {
    define('CORE_AUTOLOADER_PATH', VENDOR_PATH .
        'oxid-esales' . DIRECTORY_SEPARATOR .
        'oxideshop-ce' . DIRECTORY_SEPARATOR .
        'source' . DIRECTORY_SEPARATOR .
        'Core' . DIRECTORY_SEPARATOR .
        'Autoload' . DIRECTORY_SEPARATOR);
} else {
    define('CORE_AUTOLOADER_PATH', OX_BASE_PATH . 'Core' . DIRECTORY_SEPARATOR . 'Autoload' . DIRECTORY_SEPARATOR);
}

$GLOBALS["bShopInstalled"] = false;

if (
    file_exists(CONFIG_FILE)
    && strpos(file_get_contents(CONFIG_FILE), '<dbHost') === false
) {
    /*
    * Require and register composer autoloader.
    * This autoloader will load classes in the real existing namespace like '\OxidEsales\EshopCommunity\Core\UtilsObject'
    * It will always come first, even if you move it after the other autoloaders as it registers itself with prepend = true
    */
    require_once(VENDOR_PATH . 'autoload.php');

    /*
    * Register the backwards compatibility autoloader.
    * This autoloader will load classes for reasons of backwards compatibility like 'oxArticle'.
    */
    require_once CORE_AUTOLOADER_PATH . 'BackwardsCompatibilityAutoload.php';
    spl_autoload_register([\OxidEsales\EshopCommunity\Core\Autoload\BackwardsCompatibilityAutoload::class, 'autoload']);

    require_once(OX_BASE_PATH . 'overridablefunctions.php');
    require_once(OX_BASE_PATH . 'oxfunctions.php');

    if (class_exists('\OxidEsales\Eshop\Core\ConfigFile')) {
        $oConfigFile = new ConfigFile(OX_BASE_PATH . "config.inc.php");
        Registry::set(ConfigFile::class, $oConfigFile);
        unset($oConfigFile);

        $GLOBALS["bShopInstalled"] = true;
    }
}

/**
* ShopMaintenance Class
* can (de)activate the shop and/or the shop modules
*/
class ShopMaintenance
{
    /**
    * Composer io and compser obj
    * @type obj;
    */
    protected static $io;
    protected static $composer;

    /**
    * ignored files in image folders
    * @type array();
    */
    private static $_aIgnoredFiles = array(
        'nopics.jpg', 'dir.txt'
    );

    /**
    * ignored folders in image folders
    * @type array();
    */
    private static $_aIgnoredFolders = array(
        '.', '..'
    );

    private static $_aImageFolders = array(
        'master',
        'generated'
    );

    /**
    * Filename for modulelist
    * @param string
    */
    private static $_sTmpModulListFileName = 'modulelist.json';

    /**
    * set the Composer Class
    *
    * @param Composer $composer
    */
    public static function setComposer($composer)
    {
        self::$composer = $composer;
    }

    /**
    * set the IO Class
    *
    * @param IOInterface $io
    */
    public static function setIo($io)
    {
        self::$io = $io;
    }

    /**
    * is Installation possible
    *
    * @return mixed (boolean: true or String: Notice why not)
    */
    public static function isInstallationPossible()
    {
        if (true === ($mResult = self::isShopInstalled())) {
            if (false === ($mResult = self::isFileExistsDontRun())) {
                if (true === ($mResult = self::isFileExistsRun())) {
                    if (false === ($mResult = self::isBoolConfTrueDontRun())) {
                        if (true === ($mResult = self::isBoolConfTrueRun())) {
                            $mResult = true;
                        }
                    }
                }
            }
        }
        return $mResult;
    }

    /**
    * is Shop installed
    *
    * @return mixed (boolean: true or String: Notice why not)
    */
    public static function isShopInstalled()
    {
        return (
            $GLOBALS["bShopInstalled"] ?
            true :
            'IMPORTANT NOTE: Shop is not yet installed. Maintenance-Mode and Module-Deactivation not necessary'
        );
    }

    /**
    * is File exists: dont run
    *
    * @return mixed (boolean: false or String: Notice why not)
    */
    public static function isFileExistsDontRun()
    {
        $aExtras = self::$composer->getPackage()->getExtra();
        $aCheckFiles = $aExtras['therealworld-parameters']['housekeeping-dont-run-if-file-exists'];

        $mResult = false;
        foreach ($aCheckFiles as $sCheckFile) {
            if (file_exists($sCheckFile)) {
                $mResult = sprintf(
                    'IMPORTANT NOTE: The Installation was cancelled. The file %s exists.',
                    $sCheckFile
                );
                break;
            }
        }
        return $mResult;
    }

    /**
    * is File exists: run
    *
    * @return mixed (boolean: true or String: Notice why not)
    */
    public static function isFileExistsRun()
    {
        $aExtras = self::$composer->getPackage()->getExtra();
        $aCheckFiles = $aExtras['therealworld-parameters']['housekeeping-run-if-file-exists'];

        $mResult = true;
        foreach ($aCheckFiles as $sCheckFile) {
            if (!file_exists($sCheckFile)) {
                $mResult = sprintf(
                    'IMPORTANT NOTE: The Installation was cancelled. The file %s not exists.',
                    $sCheckFile
                );
                break;
            }
        }
        return $mResult;
    }

    /**
    * is BoolConf true: run
    *
    * @return mixed (boolean: false or String: Notice why not)
    */
    public static function isBoolConfTrueDontRun()
    {
        $aExtras = self::$composer->getPackage()->getExtra();
        $aCheckBools = $aExtras['therealworld-parameters']['housekeeping-dont-run-if-boolconf-true'];

        $mResult = false;
        foreach ($aCheckBools as $sCheckBools) {
            if (Registry::getConfig()->getConfigParam($sCheckBools)) {
                $mResult = sprintf(
                    'IMPORTANT NOTE: The Installation was cancelled. The option %s is true.',
                    $sCheckBools
                );
                break;
            }
        }
        return $mResult;
    }

    /**
    * is BoolConf true: run
    *
    * @return mixed (boolean: true or String: Notice why not)
    */
    public static function isBoolConfTrueRun()
    {
        $aExtras = self::$composer->getPackage()->getExtra();
        $aCheckBools = $aExtras['therealworld-parameters']['housekeeping-run-if-boolconf-true'];

        $mResult = true;
        foreach ($aCheckBools as $sCheckBools) {
            if (!Registry::getConfig()->getConfigParam($sCheckBools)) {
                $mResult = sprintf(
                    'IMPORTANT NOTE: The Installation was cancelled. The option %s is true.',
                    $sCheckBools
                );
                break;
            }
        }
        return $mResult;
    }

    /**
    * set the Shop Maintenance Mode ON
    *
    * @return boolean
    */
    public static function setShopMaintenanceOn()
    {
        $bResult = false;
        if (self::isInstallationPossible() === true) {
            // Maintenance Mode ON means that the shop is OFF
            $bResult = self::_setShopStatus(0);
        }
        return $bResult;
    }

    /**
    * set the Shop Maintenance Mode OFF
    *
    * @return boolean
    */
    public static function setShopMaintenanceOff()
    {
        $bResult = false;
        if (self::isInstallationPossible() === true) {
            // Maintenance Mode OFF means that the shop is ON
            $bResult = self::_setShopStatus(1);
        }
        return $bResult;
    }

    /**
    * activate the Active Modules
    *
    * @return boolean
    */
    public static function activateActiveModules()
    {
        $bResult = false;
        if (
            self::isInstallationPossible() === true
            && file_exists(self::$_sTmpModulListFileName)
        ) {
            if ($sActiveModuleInfo = file_get_contents(self::$_sTmpModulListFileName)) {
                $aActiveModuleInfo = json_decode($sActiveModuleInfo, true);

                self::_setModuleStatus($aActiveModuleInfo, 1, 'activate');
            }
            unlink(self::$_sTmpModulListFileName);
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * disable the Active Modules
    *
    * @return boolean
    */
    public static function disableActiveModules()
    {
        $bResult = false;
        if (
            self::isInstallationPossible() === true
            && !file_exists(self::$_sTmpModulListFileName)
        ) {
            $oModuleList = oxNew(ModuleList::class);

            $aActiveModuleInfo = array();
            $aTmpActiveModuleInfo = $oModuleList->getActiveModuleInfo();

            $aExtras = self::$composer->getPackage()->getExtra();
            $aVipModules = $aExtras['therealworld-parameters']['housekeeping-vip-modules'];

            // prioritize order
            foreach ($aVipModules as $sVipModule) {
                if (array_key_exists($sVipModule, $aTmpActiveModuleInfo)) {
                    $aActiveModuleInfo[$sVipModule] = $aTmpActiveModuleInfo[$sVipModule];
                    unset($aTmpActiveModuleInfo[$sVipModule]);
                }
            }
            $aActiveModuleInfo = array_merge($aActiveModuleInfo, $aTmpActiveModuleInfo);

            if (count($aActiveModuleInfo)) {
                $sActiveModuleInfo = json_encode($aActiveModuleInfo);

                // prioritize order
                $aActiveModuleInfo = array_reverse($aActiveModuleInfo);

                if (file_put_contents(self::$_sTmpModulListFileName, $sActiveModuleInfo)) {
                    self::_setModuleStatus($aActiveModuleInfo, 0, 'deactivate');
                }
            }
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * delete Demo Images from Source
    */
    public static function deleteDemoImages()
    {
        $aFilesTarget = array();
        foreach (self::$_aImageFolders as $sImageFolder) {
            $sOxidImageVendorPath = VENDOR_PICTURE_PATH . $sImageFolder;
            $sOxidImageSourcePath = SOURCE_PICTURE_PATH . $sImageFolder;

            if (
                is_dir($sOxidImageVendorPath) &&
                is_dir($sOxidImageSourcePath)
            ) {
                $aFiles = self::_getDirContents($sOxidImageVendorPath);
                foreach ($aFiles as $sFile) {
                    $sFileTarget = str_replace(DIFFERENCE_PATH, '', $sFile);
                    if (is_file($sFileTarget)) {
                        $aFilesTarget[] = $sFileTarget;
                    }
                }
            }
        }

        if (count($aFilesTarget)) {
            $aExtras = self::$composer->getPackage()->getExtra();

            $sQuestion = "Should the demo images be deleted? (y/N) ";

            if (
                $aExtras['therealworld-parameters']['housekeeping-delete-withoutprompt']
                || self::_askQuestion($sQuestion)
            ) {
                foreach ($aFilesTarget as $sFileTarget) {
                    unlink($sFileTarget);
                }
            }
            $sResult = sprintf('<info>%s Image-Files deleted</info>', count($aFilesTarget));
        } else {
            $sResult = 'No unnecessary pictures found for deletion';
        }
        self::$io->write($sResult);
    }

    /**
    * delete Modules before Install
    */
    public static function deleteModulesBeforeInstall()
    {
        $aExtras = self::$composer->getPackage()->getExtra();
        if (isset($aExtras['therealworld-parameters']['housekeeping-delete-modulevendorids'])) {
            $sQuestion = "Should the module paths be cleaned up before installation? (y/N) ";

            if (
                $aExtras['therealworld-parameters']['housekeeping-delete-withoutprompt']
                || self::_askQuestion($sQuestion)
            ) {
                foreach ($aExtras['therealworld-parameters']['housekeeping-delete-modulevendorids'] as $sVendorId) {
                    $sVendorIDPath = SOURCE_MODULE_PATH . $sVendorId;
                    if (self::_rrmdir($sVendorIDPath)) {
                        self::$io->write(sprintf('<info>Module-Vendor-Path "%s" found and deleted</info>', $sVendorId));
                    }
                }
            }
        }
    }

    /**
    * delete Themes before Install
    */
    public static function deleteThemesBeforeInstall()
    {
        $aExtras = self::$composer->getPackage()->getExtra();

        if (isset($aExtras['therealworld-parameters']['housekeeping-delete-themeids'])) {
            $sQuestion = "Should the theme paths be cleaned up before installation? (y/N) ";

            if (
                $aExtras['therealworld-parameters']['housekeeping-delete-withoutprompt']
                || self::_askQuestion($sQuestion)
            ) {
                foreach ($aExtras['therealworld-parameters']['housekeeping-delete-themeids'] as $sThemeId) {
                    $sThemePath = SOURCE_THEME_PATH . $sThemeId;
                    if (self::_rrmdir($sThemePath)) {
                        self::$io->write(sprintf('<info>Theme-Path "%s" found and deleted</info>', $sThemeId));
                    }
                    $sOutPath = SOURCE_OUT_PATH . $sThemeId;
                    if (self::_rrmdir($sOutPath)) {
                        self::$io->write(sprintf('<info>Out-Path "%s" found and deleted</info>', $sThemeId));
                    }
                }
            }
        }
    }

    /**
    * get the content of a dir
    */
    private static function _getDirContents($sDir, &$aResults = array())
    {
        $aFiles = scandir($sDir);

        foreach ($aFiles as $iKey => $sFile) {
            $sPath = realpath($sDir . DIRECTORY_SEPARATOR . $sFile);
            if (!is_dir($sPath) && !in_array($sFile, self::$_aIgnoredFiles)) {
                $aResults[] = $sPath;
            } elseif (is_dir($sPath) && !in_array($sFile, self::$_aIgnoredFolders)) {
                self::_getDirContents($sPath, $aResults);
            }
        }
        return $aResults;
    }

    /**
    * Returns true if the human answer to the given question was answered with a positive value (Yes/yes/Y/y).
    *
    * @param string $sMessageToAsk
    * @return bool
    */
    private static function _askQuestion($sMessageToAsk)
    {
        $sUserInput = self::$io->ask($sMessageToAsk, 'N');

        return self::_isPositiveUserInput($sUserInput);
    }

    /**
    * Return true if the input from user is a positive answer (Yes/yes/Y/y)
    *
    * @param string $sUserInput Raw user input
    *
    * @return bool
    */
    private static function _isPositiveUserInput($sUserInput)
    {
        $aPositiveAnswers = array('yes', 'y');

        return in_array(strtolower(trim($sUserInput)), $aPositiveAnswers, true);
    }

    /**
    * delete a folder recursivly
    *
    * @param string $sPath
    *
    * @return null
    */
    private static function _rrmdir($sPath)
    {
        $bResult = false;
        if (is_dir($sPath)) {
            $aList = scandir($sPath);
            foreach ($aList as $sFile) {
                if (!in_array($sFile, self::$_aIgnoredFolders)) {
                    if (is_dir($sPath . DIRECTORY_SEPARATOR . $sFile)) {
                        self::_rrmdir($sPath . DIRECTORY_SEPARATOR . $sFile);
                    } else {
                        unlink($sPath . DIRECTORY_SEPARATOR . $sFile);
                    }
                }
            }
            rmdir($sPath);
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * set the Shop Status ON/OFF
    *
    * @param $iStatus status (0,1)
    *
    * @return boolean
    */
    private static function _setShopStatus($iStatus = 1)
    {
        $bResult = false;
        $oConfig = Registry::getConfig();
        $oConfig->init();
        $oShop = $oConfig->getActiveShop();

        $aParams = array(
            'oxshops__oxactive' => (int)$iStatus
        );

        $oShop->assign($aParams);
        if ($oShop->save()) {
            $bResult = true;
        }
        return $bResult;
    }

    /**
    * set the active Shop Modules On/Off
    *
    * @param $aActiveModuleInfo $oModuleList->getActiveModuleInfo()
    * @param $iStatus           status (0,1)
    * @param $sStatus           statustext
    *
    * @return boolean
    */
    private static function _setModuleStatus($aActiveModuleInfo = array(), $iStatus = 1, $sStatus = '')
    {
        $bResult = false;
        foreach ($aActiveModuleInfo as $sModuleId => $sModulePath) {
            $oModule = oxNew(Module::class);
            if ($sModuleId && $oModule->load($sModuleId)) {
                try {
                    $oModuleCache = oxNew(ModuleCache::class, $oModule);
                    $oModuleInstaller = oxNew(ModuleInstaller::class, $oModuleCache);

                    if ($iStatus) {
                        $oModuleInstaller->activate($oModule);
                    } else {
                        $oModuleInstaller->deactivate($oModule);
                    }

                    self::$io->write(sprintf(
                        "%s Module %s",
                        $sStatus,
                        $sModuleId
                    ));
                } catch (\OxidEsales\Eshop\Core\Exception\StandardException $oEx) {
                    self::$io->write($oEx->getMessage());
                }
            }
        }
        $bResult = true;
        return $bResult;
    }
}
